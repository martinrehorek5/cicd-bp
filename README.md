# Continuous Integration with GitLab for Cloud Application Development

## Table of Contents
- [Continuous Integration with GitLab for Cloud Application Development](#continuous-integration-with-gitlab-for-cloud-application-development)
  - [Table of Contents](#table-of-contents)
  - [Abbreviations](#abbreviations)
  - [Introduction](#introduction)
  - [Objectives](#objectives)
  - [Prerequisites](#prerequisites)
    - [Local development](#local-development)
    - [Cloud development](#cloud-development)
  - [Architecture Overview](#architecture-overview)
    - [Components](#components)
      - [FastAPI server](#fastapi-server)
      - [Gitlab CI](#gitlab-ci)
    - [Structure of repostiory](#structure-of-repostiory)
  - [Setup and Configuration](#setup-and-configuration)
    - [GitLab Configuration](#gitlab-configuration)
    - [Azure Configuration](#azure-configuration)
    - [Project Setup](#project-setup)
  - [CI Pipeline Explanation](#ci-pipeline-explanation)
    - [What is a pipeline?](#what-is-a-pipeline)
    - [Pipelines using Gitlab](#pipelines-using-gitlab)
    - [Pipeline Stages](#pipeline-stages)
      - [Build](#build)
      - [Test](#test)
      - [Release](#release)
      - [Deploy](#deploy)
      - [Cleanup](#cleanup)
    - [Pipeline flow](#pipeline-flow)
    - [Pipeline design](#pipeline-design)
      - [Dev](#dev)
      - [Production](#production)
        - [Overall flow](#overall-flow)
        - [Diagram capturing needed jobs](#diagram-capturing-needed-jobs)
      - [Preprod](#preprod)
        - [Overall flow](#overall-flow-1)
        - [Diagram capturing needed jobs](#diagram-capturing-needed-jobs-1)
        - [Gitlab-ci-local](#gitlab-ci-local)
  - [Monitoring and Logging](#monitoring-and-logging)
  - [Troubleshooting Common Issues](#troubleshooting-common-issues)
  - [Conclusion](#conclusion)
  - [Further improvement](#further-improvement)
  - [References and inspirations](#references-and-inspirations)

## Abbreviations
MR = Merge request

## Introduction
The practical section of this thesis will focus on demonstrating Continuous Integration and related concepts.

The main focus of this demonstration will involve creating an application in the Cloud using a CI/CD pipeline on Gitlab and deploying it on Microsoft Azure.

## Objectives
The main goal is to clarify the implementation of a CI pipeline in GitLab to showcase its effectiveness in speeding up release cycles, enhancing code quality, and promoting collaboration among development teams. This comprehensive study seeks to provide a thorough understanding of the setup and functioning of CI pipelines, with the goal of encouraging their adoption in the development of cloud applications. 
Since every project is unique, each stage of the pipeline should be customised accordingly (Becvarik, 2023) .

## Prerequisites

### Local development

Device: macOS 14.2.1 (23C71)

Installed software
- Git
- IDE
- Python 3.12.1
- Docker Desktop
- Azure CLI

Additional software
- Gitlab-ci-local

### Cloud development

- Gitlab
- Microsoft Azure

## Architecture Overview

### Components

#### FastAPI server
- Located in the `server` folder. It contains source code, testing code, code for a build
- "FastAPI is a modern, fast (high-performance), web framework for building APIs with Python 3.8+ based on standard Python type hints." - [FastAPI](https://fastapi.tiangolo.com/)
- Simple server to demonstrate building, testing and deployment.

#### Gitlab CI
- Main file containing CI practices and pipeline management is located at the root of the directory with the name `.gitlab-ci.yml`.
- Individual stages and templates are located in the `gitlab-ci` folder.

### Structure of repostiory

```
.
├── .gitignore
├── .gitlab-ci.yml
├── README.md
├── azure
│   └── custom_role.json
├── doc
│   └── img
│       ├── pipeline_diagram.drawio
│       └── pipeline_diagram.png
├── env
│   ├── preprod
│   │   └── .env
│   └── prod
│       └── .env
├── gitlab-ci
│   ├── .templates.yml
│   ├── build.yml
│   ├── cleanup.yml
│   ├── deploy.yml
│   ├── release.yml
│   └── test.yml
└── server
    ├── .dockerignore
    ├── .env
    ├── Makefile
    ├── README.md
    ├── build
    │   └── Dockerfile
    ├── requirements
    │   ├── dev.txt
    │   └── prod.txt
    ├── src
    │   ├── __init__.py
    │   └── main.py
    └── test
        ├── __init__.py
        └── main.py
```


## Setup and Configuration


### GitLab Configuration

To secure secrets, I have used Gitlab CICD Variables to hide credentials.
Used variables:

- `AZURE_APP_ID` is Azure Application ID, can be found with Azure UI or CLI.
- `AZURE_SERVICE_KEY` is Azure Service Principal Key, can be created with Azure UI or CLI.
- `AZURE_TENANT_ID` is Azure Tenant ID, can be found with Azure UI or CLI.
- `RESOURCE_GROUP_NAME` is a Resource Group used in Azure, can be created with Azure UI or CLI.

- `GITLAB_CONTAINER_REGISTRY_USERNAME` is a Deploy token username from Gitlab, can be created on Gitlab.
- `GITLAB_CONTAINER_REGISTRY_PASSWORD` is a Deploy token password from Gitlab, can be created on Gitlab.

### Azure Configuration

- Create an account.
- Find the Azure Tenant ID.
- Create a Resource Group.
- Create an Azure Service Principal with a custom role in `azure/custom_role.json` or use a Contributor role.
  - : Guide on how to create [How To Easily Create An Azure Service Principal](https://www.programonaut.com/how-to-easily-create-an-azure-service-principal-step-by-step/#what-and-why)

### Project Setup

- More information about setting up the server can be found in `server/README.md`


## CI Pipeline Explanation

### What is a pipeline?
- In DevOps, a pipeline divides the software delivery process into different stages and plays a vital role in automating the process to guarantee robustness and quality. (Ibrahim, 2019)


### Pipelines using Gitlab

Gitlab gives us a way to create pipelines easily via yaml based files with main `gitlab-ci.yml`. Utilizing variables, templates and reusability can lead to efficient automation and time saving. 

### Pipeline Stages

Every project is distinct and its pipeline should mirror its uniqueness. I selected a pipeline example that includes all stages commonly found in projects. Each stage can have one or multiple jobs. 
The whole of the pipeline can be found in the diagram below. The stages are: `build`, `test`, `release`, `deploy` and `cleanup`.

#### Build
During Build, there is one job called build, in which we firstly extract the environmental files needed for a run of the server and we set a variable that will be needed in later stages. We are using the less secure build strategy, which is using Docker in Docker. Preferably way would be to use a different software such as Kaniko since Kaniko doesn't need privileged access, making it a safer alternative. I chose Docker as more users are more familiar with the technology. I am firstly pulling the latest production ready docker image from the container registry to use a technique called caching. Docker utilizes efficiently reusing layers of images in order to safe resources and time. A successful run of this job and stage should create a Docker image that is pushed to the registry of Gitlab, as well as saving the created variables for later use. It's using default rules.

#### Test
This stage has only one job called `test`. It is using the built image from previous job called `build`. We are installing requirements that are found in the `server/requirements/dev.txt` containing tools required for testing. And as the name suggests, we test the created image. We enforce that the pipeline will stop as we do not allow it to fail. It saves the test results for further inspections. It's using default rules.

#### Release
This stage contains two jobs; `relese` and `re-tag-latest`. The job `re-tag-latest` handles the re-tagging of the image that passed the testing stagge and then saving it to the container registry on Gitlab for further sourcing in the deploy stage. It's using default rules.
The job `release` creates a release of the software using tags in git. As semantic version is becoming the standard in application development, we are using a very simplified version. We create a release only if it's created on the main default branch. It's using unique rules to not re-trigger the pipeline run.

#### Deploy
During deploy stage, we have one job `deploy`. This job checks if the Container Environment exists else it creates one with the given environment type. Then it creates the container using the latest tag for the environment type. It's using default rules.

#### Cleanup
This stage and job `cleanup` is created to remove all created resources in the resource group. The main purpose was to do a clean up of the deployment for development purposes of this thesis. In a real business project, this stage should only clean up the testing environment and using a better rollout strategy for newer releases. It's using default rules.

### Pipeline flow

The possibility to change the flow or when a pipeline gets triggered is altered by the keywords `rules` and `needs` mentioned in the gitlab yaml files.
Instead of using the traditional branch per environment, which tends to create issues along the way such as cross-configuration issues and managing multiple redundant branches, I have chosen more future driven concept, which is called folder per environment ([reference](https://www.zippyops.com/why-is-branching-in-gitops-a-bad-idea)). Basically, instead of managing multiple branches and keeping them updated, we create folders (or files) representing the environments. This is highly used in the Kubernetes world using Kustomize. Given this introduction to the flow, my idea was to create three scenarios of application development.

When a developer commits and pushes code on a non-default `feature` or `dev` branch, no stage or job gets triggered. 

When a developer feels satisfied with it's code, he can create a merge request to a default branch, which is in our case `main`. This call triggers a pipeline that has all stages except one job and that's the job called `release`, as it should reflect a latest stable version of the code.
If all jobs pass, the developer and the QA team can check the behavior of the deployed server in cloud. When all parties are content with it's product, they can finally merge to the `main` branch. In order to ensure up-to-date code with `main`, we enforce the option called `Merge commit with semi-linear history` in Gitlab to be able to merge only if the `dev/feature` branch has all recent commits from `main` (Documentation about merging methods can be found [here](https://docs.gitlab.com/ee/user/project/merge_requests/methods/).)

When there is a new commit and push on the `main` branch, all stages and jobs are triggered. Resulting in newly built fully tested Docker image, a new git tag with a release, and deployment to the production. 

### Pipeline design

#### Dev

- No stage or jobs are triggered

#### Production

##### Overall flow
![Alt text](doc/img/prod.png)
##### Diagram capturing needed jobs
![Alt text](doc/img/prod-needs.png)
(Sources from Gitlab pipelines)

#### Preprod

##### Overall flow
![Alt text](doc/img/preprod.png)
##### Diagram capturing needed jobs
![Alt text](doc/img/preprod-needs.png)
(Sources from Gitlab pipelines)

##### Gitlab-ci-local
- Gitlab-ci-local can show us the job names, stages and the jobs that need to run before it. We can also use this tool to run the pipeline locally. It's a great testing tool to see if we have individual gitlab-ci files correct. Output of `gitlab-ci-local --list-all`:
```
name           description  stage    when   allow_failure  needs
build                       build    never  false      
test                        test     never  false          [build]
re-tag-latest               release  never  false          [build,test]
release                     release  never  false          [build,test]
deploy                      deploy   never  false          [build,re-tag-latest]
cleanup                     cleanup  never  false          [deploy,build]
```


## Monitoring and Logging
Monitoring run of pipelines can be done in the GUI or API. I would suggest implementing notifications as part of the monitoring process. By default, Gitlab lets you know via email if pipeline failed or it got fixed.
You may view the logs real time in the Gitlab UI, save them to artifacts and then download the whole job run.


## Troubleshooting Common Issues
Given the nature of yaml, indentation or syntax issues can arise. Re-triggering pipelines can pose a huge risk as well due to the complexity of jobs rules.
Logging can be cumbersome due to some limitations on Gitlab side.
Gitlab CI variables can shadow your own, passing variables from one stage to the other for more complex behavior is not an easy task, especially when you are depending on variables in rules section. Logging in pipelines can be crucial, use artifacts to save outputs or try to run pipelines locally using `gitlab-ci-local`.

## Conclusion
Gitlab CI pipelines should help you accelerate the development of your application in the Cloud. Creating such a pipeline can lead to automatic testing, building and deploying processes, which can ultimately fasten the production, saving time, and resources.

## Further improvement
- Complex versioning based on tags, images not referenced as latest
- Adding linting stage
- Usage of Kaniko instead of Docker

## References and inspirations
- Becvarik, D. (2023, October 31). Chronicles of CICD. Presented at the CNCF Continuous Integration (CI) Event, Heureka Group.
- https://www.programonaut.com/how-to-deploy-azure-container-apps-with-gitlab-ci-cd/
- https://blog.callr.tech/building-docker-images-with-gitlab-ci-best-practices/
- https://docs.gitlab.com/
- https://learn.microsoft.com/en-us/cli/azure/containerapp?view=azure-cli-latest#az-containerapp-up
- https://www.programonaut.com/how-to-easily-create-an-azure-service-principal-step-by-step/#what-and-why
- https://github.com/microsoft/azure-container-apps/issues/35
- https://www.zippyops.com/why-is-branching-in-gitops-a-bad-idea

Ibrahim, M. M. A., Syed-Mohamad, S. M., & Husin, M. H. (2019). Managing quality assurance challenges of DevOps through analytics. In Proceedings of the 2019 8th International Conference on Software and Computer Applications (ICSCA '19) (pp. 194–198). Association for Computing Machinery. https://doi.org/10.1145/3316615.3316670

