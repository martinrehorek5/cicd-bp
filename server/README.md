# Local Server setup for Project

This guide covers the setup process for running the application in different environments (production and pre-production), executing tests, and building Docker images for deployment.

## Prerequisites

Before proceeding, ensure you have installed the necessary software as mentioned in the root `README.md`. This typically includes:

- Python 3.8+
- Docker (for building and running containerized applications)
- Any other software listed in the root `README.md`

## Environment Setup

### Production Environment

To set up the production environment locally, follow these steps:

1. **Create a Virtual Environment**: Isolate your Python environment to avoid conflicts between dependencies.

    ```bash
    python -m venv prod-venv
    ```

2. **Activate the Virtual Environment**: Use the following command to activate the environment.

    For Unix/macOS:

    ```bash
    source prod-venv/bin/activate
    ```

    For Windows:

    ```cmd
    prod-venv\Scripts\activate.bat
    ```

3. **Install Dependencies**: Install the required production dependencies from the `prod.txt` file.

    ```bash
    pip install -r server/requirements/prod.txt
    ```

4. **Set Environment Variables**: Copy the production `.env` file to the server's source directory.

    ```bash
    cp env/prod/.env server/src/.env
    ```

5. **Run the Application**: Navigate to the server's source directory and run the application using Uvicorn.

    ```bash
    cd server/src
    uvicorn main:app --reload
    ```

### Pre-production Environment

To set up the pre-production (development) environment, repeat the steps above with modifications for the pre-production context:

1. Create a virtual environment named `preprod-venv`.
2. Activate the `preprod-venv` environment.
3. Install dependencies from `dev.txt` instead of `prod.txt`.
4. Copy the `.env` file from `env/preprod/` instead of `env/prod/`.
5. Finally, run the application.

## Testing

To run tests on the application, make sure you have activated the appropriate virtual environment (production or pre-production as needed) and follow these steps:

1. **Install Pytest**: If not already installed, add `pytest` to your environment.

    ```bash
    pip install pytest
    ```

2. **Execute Tests**: Navigate to the project root and run the tests. The `--junitxml=junit.xml` argument generates a report in JUnit XML format.

    ```bash
    pytest server/test/main.py --junitxml=junit.xml
    ```

## Build and Run Docker Image

To build and run your application as a Docker container, follow these steps:

1. **Prepare Environment Files**: Copy the appropriate environment configuration to the server directory.

    ```bash
    cp -r env/preprod/ server/
    ```

2. **Build the Docker Image**: Replace `container-registry/image-name:tag` with your actual image name and tag.

    ```bash
    docker build -f build/Dockerfile . -t container-registry/image-name:tag
    ```

3. **Run the Container**: Start a container from the built image, mapping the container's port 80 to the host's port 80.

    ```bash
    docker run -d --rm --name mycontainer -p 80:80 container-registry/image-name:tag
    ```

---

This detailed guide should help you get started with setting up, testing, and deploying your application across different environments. Adjust paths and names as necessary to match your project's structure and naming conventions.
