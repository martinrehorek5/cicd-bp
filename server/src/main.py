"""
Main file containing setup of the fastAPI server
"""
import os
from fastapi import FastAPI
from dotenv import load_dotenv
import logging

env = load_dotenv('.env')

app = FastAPI(title=f'{os.getenv("DEPLOYMENT_ENV")}-ci-bp')

logger = logging.getLogger('uvicorn.error')
logger.info('Running application with env %s.', os.getenv("DEPLOYMENT_ENV"))

@app.get("/")
async def root():
    """
    Basic root route

    Returns:
        msg: simple message
    """
    if os.getenv("DEPLOYMENT_ENV"):
        deployment_env = os.getenv("DEPLOYMENT_ENV")
    else:
        deployment_env = 'error: could not find any env file'
    msg = f"CI-BP Project part of the thesis of Martin Rehorek in {deployment_env}."
    return {"msg": msg}
