import sys
from pathlib import Path

# Add the directory containing 'src' to the Python path.
sys.path.append(str(Path(__file__).resolve().parent.parent))

import os
from fastapi import FastAPI
from fastapi.testclient import TestClient
from src.main import app
from dotenv import load_dotenv

client = TestClient(app)

env = load_dotenv('.env')

def test_read_main():
    response = client.get("/")
    if os.getenv("DEPLOYMENT_ENV"):
        deployment_env = os.getenv("DEPLOYMENT_ENV")
    else:
        deployment_env = 'error: could not find any env file'
    assert response.status_code == 200
    assert response.json() == {"msg": f"CI-BP Project part of the thesis of Martin Rehorek in {deployment_env}."}